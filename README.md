# Genero Server

## Please Create File in Root Directory .env and Fill Configure
```text
    HOST=host
    DATABASE=database
    USER=user
    PASSWORD=password
    KEY=key_for_json_web_token_credentials
```

### `npm install`
### `npm run dev`
### `npm run build`
### `npm run start`