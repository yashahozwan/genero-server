const endpoint = {
  root: "/",
  auth: "/auth",
  user: "/users",
  tester: "/testers",
  page: "/pages",
  hplc: "/hplc",
  notFound: "/*",
};

export default endpoint;
