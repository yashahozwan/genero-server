type typeSignIn = {
  email: string;
  password: string;
};

export default typeSignIn;
