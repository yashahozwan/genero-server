type typeUser = {
  id: number;
  name: string;
  email: string;
  password: string;
  createdAt: string;
};

export default typeUser;
