export class Success<T> {
  constructor(
    public error: boolean,
    public message: string | null,
    public data: T
  ) {}
}

export class Failure {
  constructor(public error: boolean, public message: string) {}
}
