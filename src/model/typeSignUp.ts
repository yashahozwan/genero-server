type typeSignUp = {
  name: string;
  email: string;
  password: string;
};

export default typeSignUp;
