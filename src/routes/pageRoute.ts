import { Router } from "express";
import { getSignupPage, setSignupPage } from "../controller/pageController";
import onlyAdmin from "../middleware/onlyAdmin";
import requiredAuth from "../middleware/requiredAuth";

const pageRoute = Router();

pageRoute.get("/", getSignupPage);

pageRoute.use(requiredAuth);
pageRoute.use(onlyAdmin);
pageRoute.patch("/", setSignupPage);

export default pageRoute;
