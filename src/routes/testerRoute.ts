import { Router } from "express";
import {
  createTester,
  deleteTester,
  getTesters,
  updateTester,
} from "../controller/testerController";
import requiredAuth from "../middleware/requiredAuth";
import onlyAdmin from "../middleware/onlyAdmin";

const testerRoute = Router();

testerRoute.use(requiredAuth);
testerRoute.route("/").post(onlyAdmin, createTester).get(getTesters);
testerRoute.patch("/update/", onlyAdmin, updateTester);
testerRoute.delete("/delete/:id", onlyAdmin, deleteTester);

export default testerRoute;
