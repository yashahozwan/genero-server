import { Router } from "express";
import {
  createHplc,
  deleteHplc,
  getHplc,
  updateHplc,
} from "../controller/hplcCotroller";
import requiredAuth from "../middleware/requiredAuth";

const hplcRoute = Router();

hplcRoute.get("/", getHplc);
hplcRoute.post("/", createHplc);
hplcRoute.patch("/:id", updateHplc);
hplcRoute.delete("/:id", deleteHplc);

export default hplcRoute;
