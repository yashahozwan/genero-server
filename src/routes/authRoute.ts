import { Router } from "express";
import { signIn } from "../controller/authController";
import { createUser } from "../controller/userController";

const authRoute = Router();
const subEndpoint = {
  signIn: "/signin",
  signUp: "/signup",
};

authRoute.post(subEndpoint.signIn, signIn);
authRoute.post(subEndpoint.signUp, createUser);

export default authRoute;
