import { Router } from "express";
import {
  createUser,
  deleteUser,
  getUser,
  getUserProfile,
  getUsers,
} from "../controller/userController";
import requiredAuth from "../middleware/requiredAuth";

const userRoute = Router();

userRoute.use(requiredAuth);
userRoute.get("/profile", getUserProfile);
userRoute.route("/").post(createUser).get(getUsers);
userRoute.route("/:id").get(getUser).delete(deleteUser);

export default userRoute;
