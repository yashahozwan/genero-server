import Joi from "joi";

const testerUpdateValidate = Joi.object({
  id: Joi.number().required(),
  name: Joi.string().required(),
});

export default testerUpdateValidate;
