import Joi from "joi";

const hplcValidate = Joi.object({
  hplcType: Joi.string().required(),
  tester: Joi.string().required(),
  start: Joi.string().required(),
  end: Joi.string().required(),
  sample: Joi.string().required(),
  lot: Joi.string().required(),
  status: Joi.string().required(),
});

export default hplcValidate;
