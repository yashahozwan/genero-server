import Joi from "joi";

const testerValidate = Joi.object({
  name: Joi.string().min(3).required(),
});

export default testerValidate;
