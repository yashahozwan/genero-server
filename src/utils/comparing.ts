import bcrypt from "bcrypt";

const comparing = async (password: string, hash: string) => {
  return await bcrypt.compare(password, hash);
};

export default comparing;
