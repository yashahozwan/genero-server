import Joi from "joi";

const testerDeleteValidate = Joi.object({
  id: Joi.number().required(),
});

export default testerDeleteValidate;
