const lowerCase = (text: string) => text.toLowerCase();

export default lowerCase;
