import Joi from "joi";

const pageSignupValidate = Joi.object({
  isEnable: Joi.boolean().required(),
});

export default pageSignupValidate;
