import Joi from "joi";

const signInValidate = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

export default signInValidate;
