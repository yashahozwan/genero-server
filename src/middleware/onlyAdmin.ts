import { NextFunction, Request, Response } from "express";
import pool from "../database/mysql";
import constants from "../constants/constants";
import { Failure } from "../model/response";
import { BAD_REQUEST } from "http-status";

const onlyAdmin = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.app.locals;

  const resultsRole = await pool().query(
    "SELECT role.role FROM role WHERE userId = ?",
    [user.id]
  );

  const rowsRole = resultsRole[0] as any;
  const { role } = rowsRole[0];

  if (role !== constants.ADMIN) {
    const response = new Failure(true, "this access is only for admin");
    return res.status(BAD_REQUEST).json(response);
  }

  next();
};

export default onlyAdmin;
