import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import pool from "../database/mysql";
import typeUser from "../model/typeUser";
import { Failure } from "../model/response";
import { INTERNAL_SERVER_ERROR, UNAUTHORIZED } from "http-status";

const requiredAuth = (req: Request, res: Response, next: NextFunction) => {
  const headers = req.headers;
  const authorization = headers.authorization;
  const unauthorized = new Failure(true, "Unauthorized");
  if (!authorization) {
    return res.status(UNAUTHORIZED).json(unauthorized);
  }

  const token = authorization?.replace("Bearer ", "");

  if (!process.env.KEY) {
    return res.status(UNAUTHORIZED).json(unauthorized);
  }

  jwt.verify(token, process.env.KEY, async (error, payload) => {
    if (error) {
      return res.status(UNAUTHORIZED).json(unauthorized);
    }

    type JWTObject = { id: number };
    const { id } = payload as JWTObject;

    try {
      const results = await pool().query("SELECT * FROM users WHERE id = ?", [
        id,
      ]);
      const rows = results[0] as any;
      req.app.locals = rows[0] as typeUser;
      next();
    } catch (error: any) {
      res
        .status(INTERNAL_SERVER_ERROR)
        .json(new Failure(true, "Restart Server"));
    }
  });
};

export default requiredAuth;
