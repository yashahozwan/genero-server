import express, { Request, Response } from "express";
import dotenv from "dotenv";
import cors from "cors";
import endpoint from "./constants/endpoint";
import authRoute from "./routes/authRoute";
import userRoute from "./routes/userRoute";
import testerRoute from "./routes/testerRoute";
import { NOT_FOUND, OK } from "http-status";
import { Failure, Success } from "./model/response";
import pageRoute from "./routes/pageRoute";
import hplcRoute from "./routes/hplcRoute";

const main = () => {
  const app = express();
  const port = process.env.PORT || 3001;

  // CONFIGURE DEFAULT MIDDLEWARE
  dotenv.config();
  app.use(cors());
  app.use(express.json());

  // SET ENDPOINT SOURCE
  app.use(endpoint.auth, authRoute);
  app.use(endpoint.user, userRoute);
  app.use(endpoint.tester, testerRoute);
  app.use(endpoint.page, pageRoute);
  app.use(endpoint.hplc, hplcRoute);

  // SET ROOT DATA
  app.get(endpoint.root, (req: Request, res: Response) => {
    const adminOnly = "Admin only";
    const forEveryone = "For everyone";
    const appendUrl = (path: string, message: string) => ({
      message,
      url: "http://localhost:3001" + path,
    });
    const data = [
      appendUrl("/auth/signin", forEveryone),
      appendUrl("/auth/signup", adminOnly),
      appendUrl("/testers", forEveryone),
      appendUrl("/testers/delete", adminOnly),
      appendUrl("/testers/delete/update", adminOnly),
    ];
    res.status(OK).json(new Success(false, "All endpoint request", data));
  });

  // SET IF URL PATH NOT PROVIDE
  app.use(endpoint.notFound, (req: Request, res: Response) => {
    const response = new Failure(true, "Url not found");
    res.status(NOT_FOUND).json(response);
  });

  app.listen(port, () =>
    console.log(`Listening on port http://localhost:${port}`)
  );
};

main();
