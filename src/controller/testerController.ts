import { Request, Response } from "express";
import pool from "../database/mysql";
import { BAD_REQUEST, CREATED, NOT_FOUND, OK } from "http-status";
import { Failure, Success } from "../model/response";
import constants from "../constants/constants";
import testerValidate from "../utils/testerValidate";
import { ResultSetHeader } from "mysql2";
import TypeTester from "../model/typeTester";
import testerDeleteValidate from "../utils/testerDeleteValidate";
import testerUpdateValidate from "../utils/testerUpdateValidate";

export const createTester = async (req: Request, res: Response) => {
  // ONLY FOR ADMIN
  try {
    const user = req.app.locals;
    const request = await testerValidate.validateAsync(req.body);

    // Save tester
    const newTester = { ...request, userId: user.id, createdAt: Date.now() };
    const resultsInsertTester = await pool().query(
      "INSERT INTO testers(name, userId, createdAt) VALUES (?, ?, ?)",
      [newTester.name, newTester.userId, newTester.createdAt]
    );
    const rowsInsertTester = resultsInsertTester[0] as ResultSetHeader;

    // Get tester and send to response
    const resultsTester = await pool().query(
      "SELECT * FROM testers WHERE id = ?",
      [rowsInsertTester.insertId]
    );
    const rowsTester = resultsTester[0] as any;
    const tester = rowsTester[0] as TypeTester;
    const response = new Success(false, null, tester);
    res.status(OK).json(response);
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const getTesters = async (req: Request, res: Response) => {
  try {
    const resultsUsers = await pool().query("SELECT * FROM testers");
    const rows = resultsUsers[0];
    const response = new Success(false, null, rows);
    res.status(CREATED).json(response);
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const updateTester = async (req: Request, res: Response) => {
  // ONLY FOR ADMIN
  try {
    const request = await testerUpdateValidate.validateAsync(req.body);
    const resultsUpdateTester = await pool().query(
      "UPDATE testers SET name = ? WHERE id = ?",
      [request.name, request.id]
    );

    const rowsUpdateTester = resultsUpdateTester[0] as ResultSetHeader;
    if (rowsUpdateTester.affectedRows === 1) {
      res.status(OK).json({ error: false, message: "Tester name updated" });
    } else {
      const response = { error: true, message: "Invalid id tester" };
      res.status(BAD_REQUEST).json(response);
    }
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const deleteTester = async (req: Request, res: Response) => {
  // ONLY FOR ADMIN
  try {
    const id = req.params.id;
    const request = await testerDeleteValidate.validateAsync({ id });
    const resultsDeleteTester = await pool().query(
      "DELETE FROM testers WHERE id = ?",
      [request.id]
    );

    const rowsDeleteTester = resultsDeleteTester[0] as ResultSetHeader;
    if (rowsDeleteTester.affectedRows === 1) {
      res.status(OK).json({ error: false, message: "Tester is removed" });
    } else {
      const response = new Failure(true, "Invalid tester id");
      res.status(NOT_FOUND).json(response);
    }
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};
