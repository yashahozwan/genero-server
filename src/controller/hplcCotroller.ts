import pool from "../database/mysql";
import { Request, Response } from "express";
import { BAD_REQUEST, CREATED, OK } from "http-status";
import { Failure, Success } from "../model/response";
import hplcValidate from "../utils/hplcValidate";
import { ResultSetHeader } from "mysql2";

export const createHplc = async (req: Request, res: Response) => {
  try {
    const request = await hplcValidate.validateAsync(req.body);
    await pool().query(
      "INSERT INTO hplc(hplcType, tester, start, end, lot, sample, status) VALUES (?, ?, ?, ?, ?, ?, ?)",
      [
        request.hplcType,
        request.tester,
        request.start,
        request.end,
        request.lot,
        request.sample,
        request.status,
      ]
    );

    res.status(CREATED).json({ error: false, message: "HPLC Created" });
  } catch (error: any) {
    res.status(BAD_REQUEST).json(new Failure(true, error.message));
  }
};

export const getHplc = async (req: Request, res: Response) => {
  try {
    const results = await pool().query("SELECT * FROM hplc");
    const rows = results[0];
    res.status(OK).json(new Success(false, null, rows));
  } catch (error: any) {
    res.status(BAD_REQUEST).json(new Failure(true, error.message));
  }
};

export const updateHplc = async (req: Request, res: Response) => {
  try {
    const request = req.body;
    const id = parseInt(req.params.id);

    if (request.tester && id) {
      await pool().query("UPDATE hplc SET tester = ? WHERE id = ?", [
        request.tester,
        id,
      ]);
    }

    if (request.start && id) {
      await pool().query("UPDATE hplc SET start = ? WHERE id = ?", [
        request.start,
        id,
      ]);
    }

    if (request.end && id) {
      await pool().query("UPDATE hplc SET end = ? WHERE id = ?", [
        request.end,
        id,
      ]);
    }

    if (request.sample && id) {
      await pool().query("UPDATE hplc SET sample = ? WHERE id = ?", [
        request.sample,
        id,
      ]);
    }

    if (request.lot && id) {
      await pool().query("UPDATE hplc SET lot = ? WHERE id = ?", [
        request.lot,
        id,
      ]);
    }

    if (request.status && id) {
      await pool().query("UPDATE hplc SET status = ? WHERE id = ?", [
        request.status,
        id,
      ]);
    }

    res.status(OK).json({ error: false, message: "updated" });
  } catch (error: any) {
    res.status(BAD_REQUEST).json(new Failure(true, error.message));
  }
};

export const deleteHplc = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    await pool().query("DELETE FROM hplc WHERE id = ?", [parseInt(id)]);
    res.status(OK).json({ error: false, message: "Deleted" });
  } catch (error: any) {
    res.status(BAD_REQUEST).json(new Failure(true, error.message));
  }
};
