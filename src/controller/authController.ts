import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import pool from "../database/mysql";
import signInValidate from "../utils/signInValidate";
import typeSignIn from "../model/typeSignIn";
import typeUser from "../model/typeUser";
import { Failure, Success } from "../model/response";
import { BAD_REQUEST, NOT_FOUND, OK } from "http-status";
import comparing from "../utils/comparing";

export const signIn = async (req: Request, res: Response) => {
  try {
    const request = (await signInValidate.validateAsync(
      req.body
    )) as typeSignIn;

    const results = await pool().query("SELECT * FROM users WHERE email = ?", [
      request.email,
    ]);

    const rows = results[0] as any;
    const user = rows[0] as typeUser;
    const invalidUserAndPassword = new Failure(
      true,
      "Invalid email or password"
    );

    if (!user) {
      return res.status(BAD_REQUEST).json(invalidUserAndPassword);
    }

    if (!(await comparing(request.password, user.password))) {
      return res.status(BAD_REQUEST).json(invalidUserAndPassword);
    }

    if (!process.env.KEY) {
      return res.status(NOT_FOUND).json();
    }

    const token = jwt.sign({ id: user.id }, process.env.KEY);
    const response = new Success(false, null, { token });
    res.status(OK).json(response);
  } catch (error: any) {
    const response = new Success(true, error.message, { token: null });
    res.status(BAD_REQUEST).json(response);
  }
};
