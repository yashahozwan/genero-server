import { Request, Response } from "express";
import pool from "../database/mysql";
import { NOT_FOUND, OK } from "http-status";
import { Failure, Success } from "../model/response";
import { ResultSetHeader } from "mysql2";
import pageSignupValidate from "../utils/pageSignupValidate";

const SIGN_UP = "signup";
export const getSignupPage = async (req: Request, res: Response) => {
  try {
    const results = await pool().query("SELECT * FROM pages WHERE name = ?", [
      SIGN_UP,
    ]);

    const rows = results[0] as any;
    const signupPage = rows[0];

    if (!signupPage) {
      const resultsCreatePage = await pool().query(
        "INSERT INTO pages(name) VALUES (?)",
        [SIGN_UP]
      );
    }

    res.status(OK).json(new Success(false, null, signupPage));
  } catch (error: any) {
    res.status(NOT_FOUND).json(new Failure(true, error.message));
  }
};

export const setSignupPage = async (req: Request, res: Response) => {
  try {
    const request = await pageSignupValidate.validateAsync(req.body);
    await pool().query("UPDATE pages SET isEnable = ? WHERE name = ?", [
      request.isEnable,
      SIGN_UP,
    ]);

    const resultsGetPage = await pool().query(
      "SELECT * FROM pages WHERE name = ?",
      [SIGN_UP]
    );
    const rowsGetPage = resultsGetPage[0] as any;
    const signupPage = rowsGetPage[0];
    res.status(OK).json(new Success(false, null, signupPage));
  } catch (error: any) {
    res.status(NOT_FOUND).json(new Failure(true, error.message));
  }
};
