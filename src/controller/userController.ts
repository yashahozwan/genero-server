import { Request, Response } from "express";
import { BAD_REQUEST, CREATED, NOT_FOUND, OK } from "http-status";
import userValidate from "../utils/userValidate";
import TypeSignUp from "../model/typeSignUp";
import { Failure, Success } from "../model/response";
import pool from "../database/mysql";
import hashing from "../utils/hashing";
import { ResultSetHeader } from "mysql2";
import TypeUser from "../model/typeUser";

export const createUser = async (req: Request, res: Response) => {
  try {
    const request = (await userValidate.validateAsync(req.body)) as TypeSignUp;
    request.password = await hashing(request.password);

    const user = { ...request, createdAt: String(Date.now()) };

    let findByEmail = await pool().query(
      "SELECT email FROM users WHERE email = ?",
      [user.email]
    );
    const rowsEmail = findByEmail[0] as any;
    const getEmail = rowsEmail[0];

    if (getEmail) {
      const response = new Failure(true, "This email address has been taken");
      return res.status(BAD_REQUEST).json(response);
    }

    const resultsUsers = await pool().query(
      "INSERT INTO users(name, email, password, createdAt) VALUES (?, ?, ?, ?)",
      [user.name, user.email, user.password, user.createdAt]
    );
    const rowsUsers = resultsUsers[0] as ResultSetHeader;

    const resultsRole = await pool().query(
      "INSERT INTO role(role, userId) VALUES (?, ?)",
      ["user", rowsUsers.insertId]
    );
    const rowsRole = resultsRole[0] as ResultSetHeader;

    const resultsMyUser = await pool().query(
      "SELECT * FROM users WHERE id = ?",
      [rowsUsers.insertId]
    );

    const rowsMyUser = resultsMyUser[0];
    const getOneUser = rowsMyUser as any;
    const myUser = getOneUser[0] as TypeUser;

    const response = new Success(false, null, myUser);
    res.status(CREATED).json(response);
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const getUsers = async (req: Request, res: Response) => {
  try {
    const results = await pool().query("SELECT * FROM users");
    const rows = results[0];
    const response = new Success(false, null, rows);
    res.status(OK).json(response);
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const getUser = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const results = await pool().query("SELECT * FROM users WHERE id = ?", [
      id,
    ]);
    const rows = results[0] as any;
    const user = rows[0];

    if (!user) {
      const response = new Failure(true, "The user with that id doesn't exist");
      return res.status(NOT_FOUND).json(response);
    }

    const response = new Success(false, null, user);
    res.status(OK).json(response);
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const deleteUser = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    await pool().query("DELETE FROM role WHERE userId = ?", [id]);
    await pool().query("DELETE FROM users WHERE id = ?", [id]);
    res.send("Deleted");
  } catch (error: any) {
    const response = new Failure(true, error.message);
    res.status(BAD_REQUEST).json(response);
  }
};

export const getUserProfile = async (req: Request, res: Response) => {
  const user = req.app.locals;

  const results = await pool().query(
    `SELECT users.id, name, email, createdAt, role
         FROM role 
            RIGHT JOIN users ON role.role 
         WHERE role.userId = users.id 
            AND users.id = ?`,
    [user.id]
  );

  const rows = results[0] as any;
  const data = rows[0];

  const response = new Success(false, null, data);
  res.status(OK).json(response);
};
